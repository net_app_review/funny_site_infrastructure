# ---------------------------------------- download and restore mysql ------------------------------ #

# Define variables
# EXECUTING_FOLDER="/mnt/fsfunniqprod/be_mysql_backup/restore"
EXECUTING_FOLDER="/tmp"
DATABASE="itlab_test2_db"
BUCKET="funniqbackups/be_backups/"
FOLDER="be_backups/"
NTH_BACKUP="1"
PASSWORD=""
SEARCHED_CONTAINER="itlab_backend_backend_database"

# Get to the executing folder 
cd ${EXECUTING_FOLDER}

# Get the latest backup file in the 
NTH_FILE=$(aws s3 ls --profile funniq_s3 ${BUCKET} --recursive | sort | tail -n ${NTH_BACKUP} | sed -n 1p | awk '{print $4}')
RESTORE_GZIP=$(echo ${NTH_FILE} | sed -e "s#^${FOLDER}##")
echo "RESTORE_GZIP:" ${RESTORE_GZIP}
aws s3 cp --profile funniq_s3 s3://${BUCKET}${RESTORE_GZIP} ${RESTORE_GZIP}

# Unzip and get the .sql file
sudo gzip -d $(pwd)/${RESTORE_GZIP}
RESTORE_SQL_FILE=$(echo ${RESTORE_GZIP} | sed -e "s/.gz//")
RESTORE_SQL_FILE=$(pwd)/${RESTORE_SQL_FILE}
echo "RESTORE_SQL_FILE:" ${RESTORE_SQL_FILE}

# Get mysql backend service container
DOCKER_CONTAINER=$(docker ps | grep ${SEARCHED_CONTAINER} | awk '{print $1}')
echo ${DOCKER_CONTAINER}

# Rstore from the file
docker exec -i ${DOCKER_CONTAINER} /usr/bin/mysql -u root --password=${PASSWORD} ${DATABASE} < ${RESTORE_SQL_FILE}

# docker exec -i ${DOCKER_CONTAINER} /usr/bin/mysql -u root --password="Cisco@123" ${DATABASE} < ${RESTORE_SQL_FILE}
