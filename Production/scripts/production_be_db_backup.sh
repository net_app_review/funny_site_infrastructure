#!/bin/bash
# ------------------------------- Backup mysqldump ---------------------------------------
# get to tmp folder on deployment server
cd /tmp/

i=$(expr $i + 1)

# Set file name
A=$(date +%M)
A=$((A/30*30))
B=$(date +"%y%m%d_%H%M%S")$A
FILE_NAME=theinteractivelab_db_prod_backup_$B.sql
S3_BUCKET="funniqbackups/be_backups/"
BACKUP_FILE=/tmp/$FILE_NAME
PASSWORD=''
DATABASE_NAME='itlab_prod_db'
CONTAINER_NAME='prod_itlab_backend_backend_database'

# Get docker container name
DOCKER_CONTAINER=$(docker ps | grep ${CONTAINER_NAME} | awk '{print $1}')

echo ${DOCKER_CONTAINER}

# Dump the sql file to host /tmp
docker exec -i $DOCKER_CONTAINER /usr/bin/mysqldump -u root --password=${PASSWORD} ${DATABASE_NAME} > $BACKUP_FILE
if [ "${?}" -eq 0 ]; then
  sudo gzip $BACKUP_FILE
  aws s3 cp --profile funniq_s3 $BACKUP_FILE.gz s3://${S3_BUCKET}
  rm $BACKUP_FILE.gz
else
  echo "Error backing up mysql"
  exit 255
fi
